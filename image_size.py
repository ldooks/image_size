from PIL import Image
import imghdr
import os

wall_path = "{}/Dropbox/Photos/wallpaper/".format(os.environ['HOME'])

for dirpath, dirnames, filenames in os.walk(wall_path):
  for image in filenames:
    image_name = os.path.join(wall_path, image)
    if imghdr.what(image_name) in ['png', 'jpg', 'jpeg']:
        width, height = Image.open(image_name).size
        if width < 3440 and height < 1440:
          print "{}'s resolution is too small! Deleting".format(image_name)
          os.remove(image_name)
